public class bucket{

	public static int[] bucket_sort(int[] bucket,int[] goBucket, int maxNumber)
	{
		int[] sorted = new int[maxNumber + 1];
		for (int i = 0; i < bucket.length; i++)
		{
			sorted[bucket[i]] = goBucket[i];
		}
		return sorted;
	}

	public static  void main(String[] args)
	{
		int[] x = {1,3,5};
		int[] y = {6,3,1};
		int[]  sorted = bucket_sort(x ,y, 5);
		for (int i = 0; i < sorted.length; i++)
		{
				System.out.println(sorted[i]);
		}
	}
}
