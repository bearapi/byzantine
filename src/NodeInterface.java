import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by umr on 5/12/16.
 */
public interface NodeInterface extends Remote {

    void received(String string, int round, int w) throws RemoteException;

}
