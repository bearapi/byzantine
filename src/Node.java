import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;

/**
 * Created by umr on 5/12/16.
 */
public class Node {

    public static void main(String[] args) {
        int registryPort = 1099;
        int n = Integer.parseInt(args[0]);
        int f = Integer.parseInt(args[1]);
        int c = Integer.parseInt(args[2]);

        for (int i = 0; i < n; i++) {
            final int I = i;
            Runnable task = () -> {
                try {
                    String addr = "rmi://localhost:" + registryPort + "/" + I;
                    Byzantine node;
                    if (I < c) {
                        node = new Byzantine(registryPort, I, n, f, 0);
                    } else {
                        node = new Byzantine(registryPort, I, n, f, 1);
                    }
                    Naming.rebind(addr, node);
                    Thread.sleep(1000);
                    node.alg();
                } catch (MalformedURLException | RemoteException | InterruptedException e) {
                    e.printStackTrace();
                    System.out.println("Something went wrong!");
                }
            };
            (new Thread(task)).start();
        }
    }
}
