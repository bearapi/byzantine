import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

class Byzantine extends UnicastRemoteObject implements NodeInterface {

    private int v;
    private int round = 1;
    private int receivedZero;
    private int receivedOne;
    private int receivedSameMsg;
    private Boolean decided = false;

    private int nodePort;
    private int n;
    private int f;
    private Registry registry;
    private List<String> links = new ArrayList<>();
    private List<Message> message = new ArrayList<>();
    private String P = "P Message";
    private String N = "N Message";


    Byzantine(int registryPort, int nodePort, int n, int f, int v) throws RemoteException {
        this.nodePort = nodePort;
        this.n = n;
        this.f = f;
        this.registry = LocateRegistry.getRegistry(registryPort);
        this.v = v;
    }

    void alg() {
        final Byzantine currentNode = this;

        new Thread(() -> {
            try {
                String[] remoteIds = currentNode.registry.list();
                Collections.addAll(links, remoteIds);
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            //do forever
            while (true) {

                broadcast(N, round, v);

                await(n, f, N);

                int nw = count(N);

                if (receivedSameMsg > (n + f) / 2.0) {
                    broadcast(P, round, nw);
                } else {
                    broadcast(P, round, 3);
                }


                if (decided) {
                    break;
                } else {
                    await(n, f, P); //n-f messages of (P, r, *)
                }

                int pw = count(P);

                if (receivedSameMsg > f) {
                    v = pw;
                    if (receivedSameMsg > 3 * f) {
                        decided(pw);
                    }
                } else {
                    Random randomNumber = new Random();
                    v = randomNumber.nextInt(2);
                }

                round += 1;

                delete(round);

            }
        }).start();
    }


    private void delete(int round) {
        Iterator<Message> iter = message.iterator();
        synchronized (message) {
            while (iter.hasNext()) {
                Message m = iter.next();
                if (m.getRound() < round) {
                    iter.remove();
                    //System.out.printf("%d removed!, ", m.getRound());
                }
            }
        }
    }

    private void decided(int w) {
        decided = true;
        System.out.format("node %d decided on %d in round %d\n", nodePort, w, round);
    }

    private void await(int n, int f, String s) {
        while (true) {
            synchronized (message) {
                if (message.stream().filter(p -> p.getString().compareTo(s) == 0 && p.getRound() == round).count() >= n - f)
                    break;
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void received(String string, int round, int w) throws RemoteException {
        synchronized (message) {
            message.add(new Message(string, round, w));
        }
    }

    private int count(String string) {
        receivedZero = 0;
        receivedOne = 0;
        synchronized (message) {
            message.stream().filter(m -> m.getString().compareTo(string) == 0 && m.getRound() == round).forEach(m -> {
                if (m.getW() == 0) {
                    receivedZero++;
                } else if (m.getW() == 1) {
                    receivedOne++;
                }
            });
        }
        if (receivedOne >= receivedZero) {
            receivedSameMsg = receivedOne;
            return 1;
        } else {
            receivedSameMsg = receivedZero;
            return 0;
        }
    }

    private void broadcast(String string, int round, int v) {
        System.out.printf("node %d broadcasting %s, %d, %d\n", nodePort, string, round, v);
        for (String link : links) {
            try {
                NodeInterface node = (NodeInterface) registry.lookup(link);
                node.received(string, round, v);
            } catch (RemoteException | NotBoundException e) {
                e.printStackTrace();
            }
        }
    }
}
