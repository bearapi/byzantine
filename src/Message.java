/**
 * Created by umr on 5/11/16.
 */
public class Message {

    private String string;
    private int round;
    private int w;


    private String P = "P Message";
    private String N = "N Message";


    public Message(String string, int round, int w) {
        this.string = string;
        this.round = round;
        this.w = w;
    }

    public String getString() {
        return string;
    }


    public int getRound() {
        return round;
    }


    public int getW() {
        return w;
    }
}






